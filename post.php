<?php include "includes/header.php"; ?>

    <!-- Navigation -->
    <?php include "includes/navigation.php"; ?>

    <?php
    // Retrieving post details from the post id comming from url
        if(isset($_GET['post-id'])) {
            $post_id = $_GET['post-id'];
            $query = "SELECT * FROM posts WHERE post_id =$post_id ";
            $postResults = mysqli_query($connection, $query);

            $row = mysqli_fetch_assoc($postResults);
            $postTitle = $row['post_title'];
            $postAuthor = $row['post_author'];
            $postDate = $row['post_date'];
            $postImageUrl = $row['post_image'];
            $postContent = $row['post_content'];

            $update_view_query = "UPDATE posts SET post_view_count = post_view_count + 1 ";
            $update_view_query .= "WHERE post_id = $post_id";
            $update_view_count = mysqli_query($connection, $update_view_query);
        } else {
            header("index.php");
        }

    //Inserting comment data into the database
    if(isset($_POST['comment-btn'])) {
        $commentField = $_POST['comment-text'];
        $commentField = mysqli_real_escape_string($connection,strip_tags($commentField));

        $date = date('Y-m-d');

        $commentAuthor = $_POST['comment-author'];
        $commentAuthor = mysqli_real_escape_string($connection, $commentAuthor);

        $commentEmail = $_POST['comment-email'];
        $commentEmail = mysqli_real_escape_string($connection, $commentEmail);

        if(!empty($commentField) && !empty($commentAuthor) && !empty($commentEmail)) {
    
            $commentQuery = "INSERT INTO comments(comment_post_id,comment_author,comment_email, comment_date, comment_content) ";
            $commentQuery .= "VALUES($post_id,'$commentAuthor','$commentEmail', '$date', '$commentField')";
            $insertComment = mysqli_query($connection, $commentQuery);
    

        } else {
            echo "<script>alert('Every field must be filled');</script>";
        }

    }
    ?>
    

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->

                <!-- Title -->
                <h1><?php echo $postTitle; ?></h1>
                <!-- Author -->
                <p class="lead">
                    by <a href="#"><?php echo $postAuthor; ?></a>
                </p> 

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $postDate; ?></p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="images/<?php echo $postImageUrl; ?>" alt="">

                <hr>

                <!-- Post Content -->
                <?php echo $postContent; ?>

                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post" action="">
                        <div class="form-group">
                            <label for="comment-author">Name</label>
                            <input type="text" class="form-control" name="comment-author"></input>
                        </div>
                        <div class="form-group">
                            <label for="comment-email">Email</label>
                            <input type="email" class="form-control" name="comment-email"></input>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="comment-text" placeholder="Your comment..."></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" name="comment-btn">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comment -->
                <?php
                //Shoing all approved comments to this post 
                    $query = "SELECT * FROM comments WHERE comment_post_id = '$post_id'
                     AND comment_status = 'Approved' 
                     ORDER BY comment_id DESC";

                    $result = mysqli_query($connection, $query);
                    

                    while($row = mysqli_fetch_assoc($result)) {
                        $commentAuthor = $row['comment_author'];
                        $commentDate = $row['comment_date'];
                        $commentContent = $row['comment_content'];

                            ?>
                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><?php echo $commentAuthor; ?>
                                            <small><?php echo $commentDate; ?></small>
                                        </h4>
                                        <?php echo $commentContent; ?>
                                    </div>
                                </div>

                        <?php } 

                ?>

                <!-- Comment -->
            </div>

            <!-- Blog Sidebar Widgets Column -->
           <?php include "includes/sidebar.php"; ?>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
