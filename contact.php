<?php  include "includes/header.php"; ?>


<!-- Navigation -->

<?php  include "includes/navigation.php"; ?>

<div class="container">
<?php 
    if(isset($_POST['submit'])) {
        $email = $_POST['email'];
        $name = $_POST['name'];
        $tunnel = $_POST['finding-us'];
        $message = $_POST['message'];

        $query = "INSERT INTO contacts (email,name,tunnel,comment) ";
        $query .= "VALUES ('$email', '$name', '$tunnel', '$message')";
        $send_contact = mysqli_query($connection, $query);


        if($send_contact) {
            echo "<div class='alert alert-success'>Thanks for your contact! I'll will be in touch soon!</div>";
        } else {
            echo "<div class='alert alert-danger '>Check the entries!</div>";
        }
    }
?>


    <form action="" method="post" class="col-sm-6">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
         <input type="email" name="email" class="form-control" required placeholder="Email">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control"  placeholder="Name" required>
        </div>
    </div>
    <fieldset class="form-group" >
        <div class="row">
            <legend class="col-form-label col-sm-6 pt-0">How did you find us?</legend>
            <div class="col-sm-10">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="finding-us" value="google" checked>
                    <label class="form-check-label" for="finding-us"> Google </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="finding-us"  value="instagram">
                    <label class="form-check-label" for="finding-us"> Instagram </label>
                </div>
                <div class="form-check disabled">
                    <input class="form-check-input" type="radio" name="finding-us" value="other">
                    <label class="form-check-label" for="finding-us"> Other </label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="form-group">
        <label for="comment">Comment:</label>
        <textarea required class="form-control" name="message" rows="5" placeholder="Say something to us...."></textarea>
    </div> 
    <div class="form-group row">
        <div class="col-sm-10">
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
    </form>

    <?php include "includes/footer.php";?>
</div>




        
  