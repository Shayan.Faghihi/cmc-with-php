
<?php include "includes/header.php"; ?>

<!-- Navigation -->
<?php include "includes/navigation.php"; ?>

<?php 
    if(isset($_POST['search-btn'])) {
        $search_term = $_POST['search-term'];
        $search_query = "SELECT * FROM posts WHERE post_tags LIKE '%$search_term%'";
        $search_result = mysqli_query($connection,$search_query);

        if($row = mysqli_fetch_assoc($search_result)) {
            $search_title = $row['post_title'];
            $search_author = $row['post_author'];
            $search_date = $row['post_date'];
            $search_content = $row['post_content'];
            $search_image = $row['post_image'];        
        } else {
            die("
                <div class='container'>

                    <div class='row'>
        
                        <!-- Blog Entries Column -->
                        <div class='col-md-8'>
                
                            <h1 class='page-header'>
                            No results have been found for $search_term
                            </h1>
                        </div>
                    </div>
                    
                </div>
            ");
        }
    }
?>



<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
               You have searched for <?php echo $search_term; ?>
            </h1>

            <!-- First Blog Post -->

      
               

                        <h2>
                            <a href="#"><?php echo $search_title; ?></a>
                        </h2>
                        <p class="lead">
                            by <a href="index.php"><?php echo $search_author; ?></a>
                        </p>
                        <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $search_date; ?></p>
                        <hr>
                        <img class="img-responsive img-rounded" src="images/<?php echo $search_image; ?>" alt="">
                        <hr>
                        <p><?php 
                            $excerpt = substr($search_content,0,200);
                            echo $excerpt;
                        ?></p>
                        <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                        <hr>
    

          
            <!-- Pager -->
            <ul class="pager">
                <li class="previous">
                    <a href="#">&larr; Older</a>
                </li>
                <li class="next">
                    <a href="#">Newer &rarr;</a>
                </li>
            </ul>

        </div>

        <!-- Blog Sidebar Widgets Column -->
       <?php include "includes/sidebar.php"; ?>

    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->
   <?php include "includes/footer.php"; ?>