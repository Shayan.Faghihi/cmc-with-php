<?php 

// redirect
function redirect($location) {
    return header("Location:" . $location);
}

// main counting function
function countDataBase ($table) {
    global $connection;
    $query = "SELECT * FROM $table";
    $result = mysqli_query($connection, $query);

    return mysqli_num_rows($result);
}

// Escaping requests
function escape($var) {
    global $connection;

    return mysqli_real_escape_string($connection, $var);
}


// Adding a new category
    function insert_category() {
        global $connection;
        if(isset($_POST['add-cat-btn'])) {
            $cat_title = mysqli_real_escape_string($connection,$_POST['add-cat-title']);

            if(!empty($cat_title)) {
                $check_stmt = mysqli_prepare($connection,"SELECT cat_title FROM categories WHERE cat_title = ?");
                mysqli_stmt_bind_param($check_stmt, "s", $cat_title);
                mysqli_stmt_execute($check_stmt);
                mysqli_stmt_bind_result($check_stmt, $check_cat_title);
                mysqli_stmt_fetch($check_stmt);
                
                if(empty($check_cat_title)) {
                    $stmt = mysqli_prepare($connection,"INSERT INTO categories(cat_title) VALUES(?)");
                     mysqli_stmt_bind_param($stmt, "s", $cat_title);
                     mysqli_stmt_execute($stmt);
                     mysqli_stmt_close($stmt);
                } else {
                    echo "<div class='alert alert-danger'>". $cat_title . " already exists in the database</div>" ;
                }
            }
        }
    }


    // Showing all categories
    function showAllCategories() {
        global $connection;
        $query = "SELECT * FROM categories ORDER BY cat_id ASC";
        $results = mysqli_query($connection, $query);

        while($row = mysqli_fetch_assoc($results)) {
            ?>
                 <tr>
                    <td><?php echo $row['cat_id']; ?></td>
                    <td><?php echo $row['cat_title']; ?></td>
                    <td><a href="categories.php?delete=<?php echo $row['cat_id']; ?>"><i class="fa fa-fw fa-times">Delete</a></td>
                    <td><a href="categories.php?edit=<?php echo $row['cat_id']; ?>"><i class="fa fa-fw fa-edit">Edit</a></td>
                </tr>
        <?php }
    }


    // Deleting a category
    function deleteCategory() {
        global $connection;
        if(isset($_GET['delete'])) {
            $to_delete_cat_id = $_GET['delete'];
            $query = "DELETE FROM categories WHERE cat_id = $to_delete_cat_id";
            $delete_query = mysqli_query($connection, $query);
            header("Location: categories.php");
        }
    }


    // Query error check
    function confirmQuery($result) {
        global $connection;
        if(!$result) {
            echo "Query failed." . mysqli_error($connection);
        }
    }



    // Checking database table status
    function checkStatus($table,$column,$value){
        global $connection;
        $query = "SELECT * FROM $table WHERE $column = '$value' ";
        $result = mysqli_query($connection, $query);
        $count = mysqli_num_rows($result);

        if($count) {
            return $count;
        } else {
            return "-";
        }
    }



    // Comments count for each specific post
    function countPostComments($postId) {
        global $connection;

        $get_comment_query = "SELECT * FROM comments WHERE comment_post_id = $postId";
        $get_comment_count = mysqli_query($connection, $get_comment_query);

        $comment_count = mysqli_num_rows($get_comment_count);
        if($comment_count) {
            return $comment_count;
        } else {
            return "-";
        }
    }



    // User Online Counting
    
    function showOnlineUsers() {
        // getting data from script.js
        if(isset($_GET['onlineUsers'])) {
            global $connection;

            if(!$connection) {
                // when it comes from javascript, the connection is not defined
                session_start();
                include "../includes/db.php";

                //Retrieving online user id and his spending time on the site
                $session = session_id();
                $time = time();
                
                // Assigning an amount of time for a logged out user
                $time_out_in_seconds = 30;
                $time_out = $time - $time_out_in_seconds;
            
                // Check the online user with existing rows in the database
                $get_session_query = "SELECT * FROM users_online WHERE session = '$session'";
                $get_session = mysqli_query($connection,$get_session_query);
                $check_online_user = mysqli_num_rows($get_session);
            
                if(!$check_online_user) {
                    // This online user has entered just now and has not been online lately
                    mysqli_query($connection,"INSERT INTO users_online (session,time) VALUES('$session','$time')");
            
                } else {
                    // Updating the online user time on the site
                    mysqli_query($connection, "UPDATE users_online SET time = '$time' WHERE session='$session'");
                }
    
    
                $count_user_query = "SELECT * FROM users_online WHERE time > $time_out";
                $count_users = mysqli_query($connection, $count_user_query);
    
                echo mysqli_num_rows($count_users);
            }

        }
    }
    showOnlineUsers();
    


    // Is user an Admin
    function is_admin($username = '') {
        global $connection;

        $query = "SELECT user_role FROM users WHERE username= '$username' ";
        $result = mysqli_query($connection, $query);
        if($row = mysqli_fetch_assoc($result)) {

            if($row['user_role'] == 'Admin') {
                return true;
            } else {
                return false;
            }
        }
        
    }


    // Check database for existing username
    function user_exists($username) {
        global $connection;

        $query = "SELECT username FROM users WHERE username= '$username' ";
        $result = mysqli_query($connection, $query);
        
        if(mysqli_num_rows($result)) {
            // The username is already taken
            return true;
        } else {
            return false;
        }
    }
    
    //Check database for existing email
    function email_exists($email) {
        global $connection;

        $query = "SELECT user_email FROM users WHERE user_email = '$email'";
        $result = mysqli_query($connection, $query);

        if(mysqli_num_rows($result)) {
            // This email is already in the database
            return true;
        } else {
            return false;

        }
    } 

    // Registration Form
    function register_user($username, $email, $password) {
        global $connection;

            $_SESSION['reg-username'] = $username;
            $_SESSION['reg-email'] = $email;
            $password = $password;
 

            if(!empty($username) && !empty($email) && !empty($password)) {

                $username = mysqli_real_escape_string($connection,$username);
                $email = mysqli_real_escape_string($connection,$email);
                $password = mysqli_real_escape_string($connection, $password);
              
               
               
                $register_query = "INSERT INTO users (username, user_email, user_password) ";
                $register_query .= "VALUES ('$username', '$email', '$password')";
                $register_user = mysqli_query($connection, $register_query);

                $_SESSION['username'] = $username;
                $_SESSION['email'] = $email;
                $_SESSION['user_role'] = "Subscriber";
               
                header("Location: index.php");
            } 
 
    }

    // Login User 
    function login_user($username, $password) {
        global $connection;

        $get_user_pass_query = "SELECT user_password FROM users WHERE username = '$username'";
        $get_user_pass = mysqli_query($connection, $get_user_pass_query);
        $expected_pass = mysqli_fetch_assoc($get_user_pass)['user_password'];

        
        if(password_verify($password,$expected_pass)) {
            $search_user_query = "SELECT * FROM users WHERE username = '$username'";
            $search_users = mysqli_query($connection, $search_user_query);
            $user_row = mysqli_fetch_assoc($search_users);
            
            $_SESSION['username'] = $user_row['username'];
            $_SESSION['firstname'] = $user_row['user_firstname'];
            $_SESSION['lastname'] = $user_row['user_lastname'];
            $_SESSION['user_role'] = $user_row['user_role'];

            header("Location: ../admin/");
        } else {
            header("Location:../index.php");
        }
        
    }
?> 

