
ClassicEditor
    .create( document.querySelector( '#body' ) )
    .catch( error => {
    console.error( error );
    });



// All box to be checked in all posts bulk option

$(document).ready(function() {
    
    $('#bulkCheckBox').click(function(){
        if(this.checked) {
            $(".checkBox").each(function() {
                this.checked = true;
            });
        } else {
            $(".checkBox").each(function(){
                this.checked = false;
            });
        }
    });

});


// Loading a loader in each page

let loadingBox = "<div id='load-screen'><div id='loading'></div></div>";
$("body").prepend(loadingBox);

$("#load-screen").delay(700).fadeOut(400,() => {
    $(this).remove();
})
    


// Showing all the online users on the fly
function loadOnlineUsers() {
    $.get("functions.php?onlineUsers=result",(result) => { 
        if(result == 1) {
            $("#onlineUsers").text("You are alone");
        } else {

            $("#onlineUsers").text(result);
        }
    });
}
// run the function every 500ms
setInterval(()=>{
    
    loadOnlineUsers();

},500);

