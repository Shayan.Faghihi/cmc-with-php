<form action="" method="post">
    <div class="form-group">
        <?php 
        if(isset($_GET['edit'])) {
            $cat_title_query = "SELECT cat_title FROM categories WHERE cat_id = $cat_id";
            $query = mysqli_query($connection,$cat_title_query);
            $row = mysqli_fetch_assoc($query);

            $cat_title = $row['cat_title'];
            ?>
            <input type="text" name="edit-cat-title" class="form-control" value="<?php echo isset($cat_title) ? $cat_title : "" ?>" placeholder="Select the category to edit">
        <?php }
        ?>

        <?php 
        //Edit category query
        if(isset($_POST['edit-cat-btn'])) {

            $cat_title = escape($_POST['edit-cat-title']);
            $cat_id = escape($_GET['edit']);

            $stmt = mysqli_prepare($connection,"UPDATE categories SET cat_title = ? WHERE cat_id = ?");
            mysqli_stmt_bind_param($stmt, "si", $cat_title, $cat_id);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            redirect("categories.php");

        }
        ?>
    </div>
    <div class="form-group">
        <input type="submit" name="edit-cat-btn" class="btn btn-primary" value="EDIT">
    </div>
</form>