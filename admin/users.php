<?php include "includes/header.php"; ?>
    <?php 
        // Check users ----> if he is not admin redirect to the index page
        if(!is_admin($_SESSION['user_role'])) {
            header("Location: index.php");
        }
    ?>
    <div id="wrapper">
      <!-- Navigation -->
        <?php include "includes/navigation.php"; ?>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Users
                            <small>Author Name</small>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

            <!-- /.container-fluid -->
            <div class="col-xs-6">

               <?php 
                    if(isset($_GET['source'])) {
                        $source = $_GET['source'];
                    } else {
                        $source = '';
                    }

                        switch($source) {

                            case 'edit-user':
                                include "includes/edit_user.php";
                            break;

                            case 'add-new-user':
                                include "includes/add_user.php";
                            break;

                            default :
                                include "includes/view_all_users.php";
                        }
                    
               ?>

            </div>


            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php include "includes/footer.php"; ?>