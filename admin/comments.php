<?php include "includes/header.php"; ?>
    <div id="wrapper">
      <!-- Navigation -->
        <?php include "includes/navigation.php"; ?>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Comments
                            <small>Author Name</small>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

            <!-- /.container-fluid -->
            <div class="col-xs-6">

                <?php
                    if(isset($_GET['source'])) {
                        $source = $_GET['source'];
                    } else {
                        $source = '';
                    }

                        
                        switch ($source) {

                            case 'edit-comment':
                                include "includes/edit_comment.php"; 
                            break;

                            default:
                            include "includes/view_all_comments.php";
                      
                        }
                    
                ?>

            </div>


            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php include "includes/footer.php"; ?>