<?php include "includes/header.php"; ?>
    <div id="wrapper">
      <!-- Navigation -->
        <?php include "includes/navigation.php"; ?>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Profile of
                            <small><?php echo $user_logged_in; ?></small>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

                <?php 
                // Getting user's data through session
                    $get_user_query = "SELECT * FROM users WHERE username = '$user_logged_in'";
                    $get_users = mysqli_query($connection, $get_user_query);
                    while($row = mysqli_fetch_assoc($get_users)) {
                        $get_user_id = $row['user_id'];
                        $get_user_firstname = $row['user_firstname'];
                        $get_user_lastname = $row['user_lastname'];
                        $get_user_email = $row['user_email'];
                        $get_user_image = $row['user_image'];
                    }

                ?>

                <?php 
                    // Updating user data after changing
                    if(isset($_POST['update-user-btn'])) {
                        $username = $_POST['username'];
                        $user_firstname = $_POST['user-firstname'];
                        $user_lastname = $_POST['user-lastname'];
                        $user_password = $_POST['user-password'];
                        $user_email = $_POST['user-email'];
                        $user_role = $_POST['user-role'];
                
                        //Retrieving user profile image
                        $user_image = $_FILES['user-image']['name'];
                        $user_image_tmp = $_FILES['user-image']['tmp_name'];
                        $user_image_size = $_FILES['user-image']['size'];
                
                    
                
                        if($user_image_size > 5 * 1024 * 1024) {
                            echo "<div class='alert alert-danger'>The image size cannot be more than 5MB</div>";
                        } else {
                
                            if(empty($user_image)) {
                                $get_image_query = "SELECT user_image FROM users WHERE user_id = $user_to_edit_id";
                                $result = mysqli_query($connection, $get_image_query);
                                $user_image = mysqli_fetch_assoc($result)['user_image'];
                            }
                
                            move_uploaded_file($user_image_tmp,"../images/$user_image");
                    
                            //Inserting new user data to the database
                            $edit_user_query = "UPDATE users SET ";
                            $edit_user_query .= "username = '$username', ";
                            $edit_user_query .= "user_firstname = '$user_firstname', ";
                            $edit_user_query .= "user_lastname = '$user_lastname', ";
                            $edit_user_query .= "user_password = '$user_password', ";
                            $edit_user_query .= "user_email = '$user_email', ";
                            $edit_user_query .= "user_role = '$user_role', ";
                            $edit_user_query .= "user_image = '$user_image' ";
                            $edit_user_query .= " WHERE user_id = $get_user_id";
                    
                            $insertUser = mysqli_query($connection, $edit_user_query); 
                
                            if(!confirmQuery($insertUser)) {
                                echo "<div class='alert alert-success'>The user has been updated successfully. <a href='users.php' class='btn btn-info'>Check users</a></div>";
                            }
                        }
                
                    }
                
                ?>

            <!-- /.container-fluid -->
            <div class="col-xs-6">
            <form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <img width="200" src="../images/<?php echo $get_user_image; ?>" alt="" class="img-thumbnail">
                    <input type="file" name="user-image" class="form-control" accept="image/png, image/jpeg">
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input value="<?php echo $user_logged_in; ?>" type="text" name="username" class="form-control">
                </div>
                <div class="form-group">
                    <label for="user-firstname">Firstname</label>
                    <input value="<?php echo $get_user_firstname; ?>" type="text" name="user-firstname" class="form-control">
                </div>
                <div class="form-group">
                    <label for="user-lastname">Lastname</label>
                    <input value="<?php echo $get_user_lastname; ?>" type="text" name="user-lastname" class="form-control">
                </div>
                <div class="form-group">
                    <label for="user-password">Change Password</label>
                    <input autocomplete="off" type="password" name="user-password" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="user-email">Email</label>
                    <input value="<?php echo $get_user_email; ?>" type="email" name="user-email" class="form-control">
                </div>
            
                <div class="form-group">
                    <input type="submit" name="update-user-btn" value="UPDATE" class="btn btn-primary">
                </div>

            </form>

             
            </div>


            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php include "includes/footer.php"; ?>