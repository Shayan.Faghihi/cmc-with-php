<?php include "includes/header.php"; ?>
    <div id="wrapper">
      <!-- Navigation -->
        <?php include "includes/navigation.php"; ?>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to the dashboard
                            <small>Author Name</small>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

            <!-- /.container-fluid -->
            <div class="col-xs-6">

                <?php insert_category();?>

              <!-- Add New category form -->
                <form action="" method="post">
                    <div class="form-group">
                        <label for="cat-title">Add a category</label>
                        <input type="text" name="add-cat-title" class="form-control">

                    </div>
                    <div class="form-group">
                        <input type="submit" name="add-cat-btn" class="btn btn-primary" value="Add Category">
                        
                        
                    </div>
                </form>

                <!-- Edit category form -->
                    <?php
                        if(isset($_GET['edit'])) {
                            $cat_id = $_GET['edit'];
                            include "update_categories.php";
                        }            
                    ?>
            </div>
            <div class="col-xs-6">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Category Title</th>
                        </tr>
                    </thead>
                    <tbody>

                         <?php 
                            // Show all categories in the table
                            showAllCategories();


                           //Deleting categories query
                           deleteCategory();
                        ?>     
                     
                    </tbody>
                </table>
            </div>


            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include "includes/footer.php"; ?>