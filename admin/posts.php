<?php include "includes/header.php"; ?>
    <div id="wrapper">
      <!-- Navigation -->
        <?php include "includes/navigation.php"; ?>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Posts
                            <small>Author Name</small>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

            <!-- /.container-fluid -->
            <div class="col-xs-6">

                <?php
                    if(isset($_GET['source'])) {
                        $source = $_GET['source'];
                    } else {
                        $source = '';
                    }

                        //every post condition will be a branch of posts.php?source=
                        switch ($source) {
                            case 'add-new-post':
                                include "includes/add_post.php";
                            break;

                            case 'edit-post':
                                include "includes/edit_post.php"; 
                            break;

                            case 'view-comment':
                                include "includes/post_comments.php";
                            break;

                            default:
                            include "includes/view_all_posts.php";
                      
                        }
                    
                ?>

            </div>


            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php include "includes/footer.php"; ?>


