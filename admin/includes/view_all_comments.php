
<table class="table table-bordered table-hover">            
    <thead>
        <tr>
            <th>Id</th>
            <th>In Response To</th>
            <th>User</th>
            <th>User Email</th>
            <th>Date</th>
            <th>Comment Content</th>
            <th>Comment Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        // Showing all comments in dashboard
            $show_comments_query = "SELECT * FROM comments";
            $show_comments = mysqli_query($connection, $show_comments_query);
            while ($row = mysqli_fetch_assoc($show_comments)) {
                $post_id = $row['comment_post_id'];
                ?>
                    <tr>
                        <td><?php echo $row['comment_id']; ?></td>
                        
                            <?php
                                //Showing post title based on receiving post id
                                $post_id = $row['comment_post_id'];
                                $query = "SELECT post_title FROM posts WHERE post_id = $post_id";
                                $result = mysqli_query($connection, $query);
                                echo "<td><a href='../post.php?post-id=$post_id'>" . $relatedPostTitle = mysqli_fetch_assoc($result)['post_title'] ."</a></td>";
                                
                            ?>
                
                       
                        <td><?php echo $row['comment_author']; ?></td>
                        <td><?php echo $row['comment_email']; ?></td>
                        <td><?php echo $row['comment_date']; ?></td>
                        <td><?php echo $row['comment_content']; ?></td>
                        <td><?php echo $row['comment_status']; ?></td>

                        <td><a href="comments.php?approve=<?php echo $row['comment_id']; ?>"><i class="fa fa-fw fa-edit"></i>Approve</a></td>
                        <td><a href="comments.php?decline=<?php echo $row['comment_id']; ?>"><i class="fa fa-fw fa-edit"></i>Decline</a></td>
                        <td><a href="comments.php?delete=<?php echo $row['comment_id']; ?>"><i class="fa fa-fw fa-times"></i>Delete</a></td>
                    </tr>
            <?php }

            //Approving comment 
                if(isset($_GET['approve'])) {
                    $approve_comment_id = $_GET['approve'];
                    $query = "UPDATE comments SET comment_status = 'Approved' WHERE comment_id = $approve_comment_id";
                    $approveComment = mysqli_query($connection, $query);
                    header("Location:comments.php");
                }

            //Declining comment
                if(isset($_GET['decline'])) {
                    $decline_comment_id = $_GET['decline'];
                    $query = "UPDATE comments SET comment_status = 'Declined' WHERE comment_id = $decline_comment_id";
                    $declineComment = mysqli_query($connection, $query);
                    header("Location:comments.php");
                }
                
            // deleting a comment after clicking the times icon
                if(isset($_GET['delete'])) {
                    $delete_comment_id = $_GET['delete'];
                    $delete_query = "DELETE FROM comments WHERE comment_id = $delete_comment_id";
                    $delete_comment = mysqli_query($connection,$delete_query);

                    header("location:comments.php");

                }
        ?>
    </tbody>
</table>
