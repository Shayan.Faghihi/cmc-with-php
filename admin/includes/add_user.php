<?php

    if(isset($_POST['add-user-btn'])) {
        $username = $_POST['username'];
        $user_firstname = $_POST['user-firstname'];
        $user_lastname = $_POST['user-lastname'];
        $user_email = $_POST['user-email'];
        $user_role = $_POST['user-role'];

        $user_password = $_POST['user-pass'];
        $user_password = password_hash($user_password, PASSWORD_BCRYPT, ["cost" => 10]);

        $user_image = $_FILES['user-image']['name'];
        $user_image_tmp = $_FILES['user-image']['tmp_name'];
        $user_image_size = $_FILES['user-image']['size'];

      
        if($user_image_size > 5 * 1024 * 1024) {
           echo "<div class='alert alert-danger'>You cannot upload image with size more than 5MB</div>";
        } else {

            move_uploaded_file($user_image_tmp,"../images/$user_image");
            $create_user = "INSERT INTO users(username, user_password, user_firstname, user_lastname, user_email, user_image, user_role) ";
            $create_user .= "VALUES('$username','$user_password', '$user_firstname', '$user_lastname','$user_email', '$user_image', '$user_role')";
            $query = mysqli_query($connection, $create_user);

            if(!confirmQuery($query)) {
                header("location:users.php");
            }

        }

    }
?>


<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" name="username" class="form-control">
    </div>
    <div class="form-group">
        <label for="user-firstname">First Name</label>
        <input type="text" name="user-firstname" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="user-lastname">Last Name</label>
        <input type="text" name="user-lastname" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="user-pass">Password</label>
        <input type="password" name="user-pass" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="user-email">Email</label>
        <input type="email" name="user-email" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="user-role">User Role</label>
        <select name="user-role" class="form-control">
            <option value="Subscriber">Select User Role</option>
            <option value="Admin">Admin</option>
            <option value="Author">Author</option>
            <option value="Editor">Editor</option>
            <option value="Subscriber">Subscriber</option>
        </select>
    </div>
    <div class="form-group">
        <label for="post-image">Profile Image</label>
        <input type="file" name="user-image" class="form-control" accept="image/png, image/jpeg">  
    </div>

    <div class="form-group">
        <button class="btn btn-primary" type="submit" name="add-user-btn">SUBMIT</button>
    </div>

</form>