
<table class="table table-bordered table-hover">            
    <thead>
        <tr>
            <th>Id</th>
            <th>Profile Image</th>
            <th>Username</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>User Role</th>
        </tr>
    </thead>
    <tbody>
        <?php
        // Showing all users in dashboard
            $userQuery = "SELECT * FROM users ORDER BY user_id ASC";
            $showUsers = mysqli_query($connection, $userQuery);
            while($row=mysqli_fetch_assoc($showUsers)) {
                $userId = $row['user_id'];
                $username = $row['username'];
                $firstName = $row['user_firstname'];
                $lastName = $row['user_lastname'];
                $email = $row['user_email'];
                $profileImage = $row['user_image'];
                $userRole = $row['user_role'];
                ?>
                    <tr>
                        <td><?php echo $userId; ?></td>
                        <td>
                            <img width="100" src="../images/<?php echo $profileImage; ?>" class='img-thumbnail'>
                        </td>
                        <td><?php echo $username; ?></td>
                        <td><?php echo $firstName; ?></td>
                        <td><?php echo $lastName; ?></td>
                        <td><?php echo $email; ?></td>
                        <td><?php echo $userRole; ?></td>
                        <td><a href="?source=edit-user&user-id=<?php echo $userId; ?>">Edit</a></td>
                        <td><a href="?delete&user-id=<?php echo $userId; ?>">Delete</a></td>
                        <td><a href="?to-admin&user-id=<?php echo $userId; ?>">To Admin</a></td>
                    </tr>
             <?php }
           ?>
    </tbody>
</table>



<?php 
// Delete a user after clicking
    if(isset($_GET['delete'])) {
        $user_to_delete_id = $_GET['user-id'];
        $query = "DELETE FROM users WHERE user_id = $user_to_delete_id";
        $delete_user = mysqli_query($connection,$query);
        header("Location:users.php");
    }

?>

<?php 
    // Change a user to admin if already not
    if(isset($_GET['to-admin'])) {
        $to_admin_id = $_GET['user-id'];

        $checkQuery = "SELECT user_role FROM users WHERE user_id = $to_admin_id";
        $checkUser = mysqli_query($connection, $checkQuery);
        $row = mysqli_fetch_assoc($checkUser);

        if($row['user_role'] !== 'Admin') {
            $toAdminQuery = "UPDATE users SET user_role = 'Admin' WHERE user_id = $userId";
            $toAdmin = mysqli_query($connection, $toAdminQuery);
            header("Location:users.php");
            
        } else {
            echo "<div class='alert alert-danger'>This user is already an admin</div>";
        }
    }

?>