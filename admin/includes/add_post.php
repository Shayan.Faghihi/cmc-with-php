<?php
    //Add post page and edit post page are similar
    if(isset($_POST['add-post-btn'])) {
        $post_title = $_POST['post-title'];
        $post_cat_id = $_POST['post-category'];
        $post_author = $_POST['post-author'];
        $post_status = $_POST['post-status'];

        $post_image = $_FILES['post-image']['name'];
        $post_image_tmp = $_FILES['post-image']['tmp_name'];
        $post_image_size = $_FILES['post-image']['size'];

        $post_tags = $_POST['post-tags'];
        $post_content = $_POST['post-content'];
        $post_date = date('Y-m-d');

        if($post_image_size > 5 * 1024 * 1024) {
           echo "<div class='alert alert-danger'>You cannot upload image with size more than 5MB</div>";
        } else {

            move_uploaded_file($post_image_tmp,"../images/$post_image");
            $create_post = "INSERT INTO posts (post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status) ";
            $create_post .= "VALUES('$post_cat_id','$post_title', '$post_author', '$post_date','$post_image', '$post_content', '$post_tags','$post_status')";
            $query = mysqli_query($connection, $create_post);

            if(!confirmQuery($query)) {
                $new_post_id = mysqli_insert_id($connection);   //The last post id we have created
                echo "<div class='alert alert-success'>Congratulations! Your post has been created! View it <a href='../post.php?post-id=$new_post_id'>now.</a></div>";
            }

        }

        

    }
?>


<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="post-title">Post Title</label>
        <input type="text" name="post-title" class="form-control">
    </div>
    <div class="form-group">
        <label for="post-category">Category</label>
        <!-- Select between existing categories -->
        <select name="post-category" class="form-control">
            <option value="">Select a Category</option>
            <?php 
                $showCategoriesQuery = "SELECT * FROM categories";
                $show_categories = mysqli_query($connection, $showCategoriesQuery);
                while($row = mysqli_fetch_assoc($show_categories)) {
                    $cat_title = $row['cat_title'];
                    $cat_id = $row['cat_id'];
                    ?>
                    <option value="<?php echo $cat_id; ?>"><?php echo $cat_title; ?></option>
                <?php }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label for="post-author">Post Author</label>
        <!-- Select between existing authors -->
        <select name="post-author" class="form-control">
            <option value="">Select an Author</option>
            <?php 
                $showAuthorsQuery = "SELECT * FROM users";
                $show_users = mysqli_query($connection, $showAuthorsQuery);
                while($row = mysqli_fetch_assoc($show_users)) {
                    $user_title = $row['username'];
                    ?>
                    <option value="<?php echo $user_title; ?>"><?php echo $user_title; ?></option>
                <?php }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label for="post-status">Post Status</label>
        <select name="post-status" class="form-control">
            <option value="Draft">Draft</option>
            <option value="Published">Published</option>
        </select>
    </div>

    <div class="form-group">
        <label for="post-image">Post Featured Image</label>
        <input type="file" name="post-image" class="form-control" accept="image/png, image/jpeg">
        
    </div>

    <div class="form-group">
        <label for="post-tags">Post Tags</label>
        <input type="text" name="post-tags" class="form-control">
    </div>

    <div class="form-group">
        <label for="post-content">Post Content</label>
        <textarea name="post-content" id="body" cols="30" rows="10" class="form-control"></textarea>
    </div>
    
    <div class="form-group">
        <button class="btn btn-primary" type="submit" name="add-post-btn">SUBMIT</button>
    </div>

</form> 