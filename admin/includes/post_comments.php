<?php 
    if(isset($_GET['post_id'])) {
        $post_id = mysqli_real_escape_string($connection,$_GET['post_id']);
    }
?>
<h2>Comments to this post</h2>
<table class="table table-bordered table-hover">            
    <thead>
        <tr>
            <th>Id</th>
            <th>In Response To</th>
            <th>User</th>
            <th>User Email</th>
            <th>Date</th>
            <th>Comment Content</th>
            <th>Comment Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        // Showing all comments in dashboard
            $show_comments_query = "SELECT * FROM comments WHERE comment_post_id = $post_id";
            $show_comments = mysqli_query($connection, $show_comments_query);
            while ($row = mysqli_fetch_assoc($show_comments)) {
                $related_post_id = $row['comment_post_id'];
                ?>
                    <tr>
                        <td><?php echo $row['comment_id']; ?></td>
                        
                            <?php
                                //Showing post title based on receiving post id
                                
                                $query = "SELECT post_title FROM posts WHERE post_id = $related_post_id";
                                $result = mysqli_query($connection, $query);
                                echo "<td><a href='../post.php?post-id=$related_post_id'>" . $relatedPostTitle = mysqli_fetch_assoc($result)['post_title'] ."</a></td>";
                                
                            ?>
                
                       
                        <td><?php echo $row['comment_author']; ?></td>
                        <td><?php echo $row['comment_email']; ?></td>
                        <td><?php echo $row['comment_date']; ?></td>
                        <td><?php echo $row['comment_content']; ?></td>
                        <td><?php echo $row['comment_status']; ?></td>

                        <td><a href="posts.php?source=view-comment&approve=<?php echo $row['comment_id']; ?>&post_id=<?php echo $post_id; ?>"><i class="fa fa-fw fa-edit"></i>Approve</a></td>
                        <td><a href="posts.php?source=view-comment&decline=<?php echo $row['comment_id']; ?>&post_id=<?php echo $post_id ?>"><i class="fa fa-fw fa-edit"></i>Decline</a></td>
                        <td><a href="posts.php?source=view-comment&delete=<?php echo $row['comment_id']; ?>&post_id=<?php echo $post_id; ?>"><i class="fa fa-fw fa-times"></i>Delete</a></td>
                    </tr>
            <?php }

            //Approving comment 
                if(isset($_GET['approve'])) {
                    $approve_comment_id = mysqli_real_escape_string($connection,$_GET['approve']);
                    $query = "UPDATE comments SET comment_status = 'Approved' WHERE comment_id = $approve_comment_id";
                    $approveComment = mysqli_query($connection, $query);
                    header("Location:posts.php?source=view-comment&post_id=$post_id");
                }

            //Declining comment
                if(isset($_GET['decline'])) {
                    $decline_comment_id = mysqli_real_escape_string($connection,$_GET['decline']);
                    $query = "UPDATE comments SET comment_status = 'Declined' WHERE comment_id = $decline_comment_id";
                    $declineComment = mysqli_query($connection, $query);
                    header("Location:posts.php?source=view-comment&post_id=$post_id");
                }
                
            // deleting a comment after clicking the times icon
                if(isset($_GET['delete'])) {
                    $delete_comment_id = mysqli_real_escape_string($connection,$_GET['delete']);
                    $delete_query = "DELETE FROM comments WHERE comment_id = $delete_comment_id";
                    $delete_comment = mysqli_query($connection,$delete_query);

                    header("location:posts.php?source=view-comment&post_id=$post_id");

                }
        ?>
    </tbody>
</table>
