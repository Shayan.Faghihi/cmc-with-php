<?php include "delete_modal.php"; ?>
<?php 
    //bulk options
    if(isset($_POST['checkBoxArray'])) {
        $checkeBoxArray = $_POST['checkBoxArray'];
        $bulk_select = $_POST['bulkSelect'];

       foreach($checkeBoxArray as $checkedPostId) {
            switch($bulk_select) {
                // publishing posts in bulk
                case "bulk-publish":
                    $query = "UPDATE posts SET post_status = 'Published' WHERE post_id = $checkedPostId";
                    $publishPosts = mysqli_query($connection, $query);
                break;

                // drafting posts in bulk
                case "bulk-draft":
                    $query = "UPDATE posts SET post_status = 'Draft' WHERE post_id = $checkedPostId";
                    $DraftPosts = mysqli_query($connection, $query);
                break;

                // deleting posts in bulk
                case "bulk-delete":
                    $query = "DELETE FROM posts WHERE post_id = $checkedPostId";
                    $deletehPosts = mysqli_query($connection, $query);
                break;

                // Cloning a posts in  bulk
                case "bulk-clone":
                    $query = "INSERT INTO posts (post_category_id,post_title, post_author,post_date,post_image,post_content,post_tags) ";
                    $query .= "SELECT post_category_id,post_title, post_author,now(),post_image,post_content,post_tags FROM posts ";
                    $query .= "WHERE post_id = $checkedPostId";
                    $clone = mysqli_query($connection, $query);
                break; 

                // Reseting number of views
                case 'bulk-reset-view':
                    $query = "UPDATE posts SET post_view_count = 0 ";
                    $query .= "WHERE post_id = $checkedPostId";
                    $reset_view = mysqli_query($connection, $query);
                break;
                
            }
       }
    }
?>



<form action="" method="post">
    <table class="table table-bordered table-hover">            
    <div id="selectBulkContainer" class="col-xs-4">
        <select name="bulkSelect" id="" class="form-control">
            <option value="">Select Options</option>
            <option value="bulk-publish">Publish</option>
            <option value="bulk-draft">Draft</option>
            <option value="bulk-delete">Delete</option>
            <option value="bulk-clone">Clone</option>
            <option value="bulk-reset-view">Reset Views</option>
        </select>
    </div>
    <div class="col-xs-4">
        <button class="btn btn-success" type="submit">Select</button>
        <a href="posts.php?source=add-new-post" class="btn btn-primary">Add New Post</a>
    </div>
        <thead>
            <tr>
                <th ><input type="checkbox" id="bulkCheckBox" class="form-check-input"></th>
                <th >Id</th>
                <th>Post Title</th>
                <th>Author</th>
                <th>Status</th>
                <th>Featured Image</th>
                <th>Categories</th>
                <th>Tags</th>
                <th>Comments</th>
                <th>Date</th>
                <th>Views</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // Showing all posts in dashboard
                if($user_logged_in_role !== 'Admin') {
                    // Showing each specific user's posts
                    $show_posts_query = "SELECT * ";
                    $show_posts_query .= "FROM posts LEFT JOIN categories ON posts.post_category_id = categories.cat_id  ";
                    $show_posts_query .= "WHERE posts.post_author='$user_logged_in' ORDER BY posts.post_id DESC  ";
                    
                } else if ($user_logged_in_role == 'Admin') {
                    // Show all user's posts to the admin
                    $show_posts_query = "SELECT * ";
                    $show_posts_query .= "FROM posts LEFT JOIN categories ON posts.post_category_id = categories.cat_id  ";
                    $show_posts_query .= " ORDER BY posts.post_id DESC  ";
                }

                $show_posts = mysqli_query($connection, $show_posts_query);
                while ($row = mysqli_fetch_assoc($show_posts)) {
                    $post_id = $row['post_id'];
                    $post_title = $row['post_title'];
                    $post_author = $row['post_author'];
                    $post_status = $row['post_status'];
                    $post_image = $row['post_image'];
                    $post_tags = $row['post_tags'];
                    $post_date = $row['post_date'];
                    $post_view = $row['post_view_count'];
                    $cat_title = $row['cat_title'];

                    ?>
                        <tr>
                        <!-- Putting each selected post id into an array named checkBoxArrat -->
                        <!-- Using the name of the input the value can be sent through a POST request -->
                            <td><input type="checkbox" name="checkBoxArray[]" value="<?php echo $post_id; ?>" class="form-check-input checkBox"></td>
                            <td><?php echo $post_id; ?></td>
                            <td><a href="../post.php?post-id=<?php echo $post_id; ?>"><?php echo $post_title; ?></a></td>
                            <td><?php echo $post_author; ?></td>
                            <td><?php echo $post_status; ?></td>
                            <td><img class="img-thumbnail rounded" src="../images/<?php echo $post_image; ?>"></td>
                            <td>
                                <?php
                                //Relating categories to posts and displayin it from each cat id in the while loop
                                    if($cat_title) {
                                        echo $cat_title;

                                    } else {
                                        echo "";
                                    }
                                ?>
                            </td>
                            <td><?php echo $post_tags; ?></td>

                            <!-- Counting each post comment from comments database -->
                            <td> <a href="posts.php?source=view-comment&post_id=<?php echo $post_id; ?>"><?php echo countPostComments($post_id); ?> </a></td>                                
                            
                            <td><?php echo $post_date; ?></td>
                            <td><?php echo $post_view; ?></td>
                            <td><a href="posts.php?source=edit-post&edit=<?php echo $post_id; ?>"><i class="fa fa-fw fa-edit"></a></td>
                            <td><a rel='<?php echo $post_id; ?>' href='javascript:void(0)' class='delete_link' data-toggle="modal" data-target="#exampleModalCenter">Delete</a></td>

                        </tr>
                <?php }

                // deleting a post after clicking the times icon
                if(isset($_GET['delete'])) {
                    $delete_post_id = mysqli_real_escape_string($connection,$_GET['delete']);

                    $get_post_author = "SELECT post_author FROM posts WHERE post_id = $delete_post_id";
                    $post_author_result = mysqli_query($connection, $get_post_author);
                    $post_author = mysqli_fetch_assoc($post_author_result)['post_author'];

                    if($post_author !== $user_logged_in && $user_logged_in !== "Admin") {
                        die("<div class='alert alert-danger'>Not allowed</div>");
                    }

                        $delete_query = "DELETE FROM posts WHERE post_id = $delete_post_id";
                        $delete_post = mysqli_query($connection,$delete_query);
                        header("location:posts.php?source=view-all-posts");
    
                      confirmQuery($delete_post);
                    
                }
            ?>
        </tbody>
    </table>
</form>


<script>
    $(document).ready(() => {
        let deleteBtn = $(".delete_link");
        
        deleteBtn.on("click", function() {
            let id = $(this).attr("rel");
            let deleteUrl = "posts.php?delete=" + id + " ";

            $("#modal_delete_link").attr("href", deleteUrl);
        });
    });



</script>