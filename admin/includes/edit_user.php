<?php
// Getting Value from user table
if(isset($_GET['user-id'])) {
    $user_to_edit_id = $_GET['user-id'];

    $getQuery = "SELECT * FROM users WHERE user_id = $user_to_edit_id";
    $getResults = mysqli_query($connection, $getQuery);
    $row = mysqli_fetch_assoc($getResults);

    //Assigning variable to each user data
    $get_username = $row['username'];
    $get_user_firstname = $row['user_firstname'];
    $get_user_lastname = $row['user_lastname'];
    $get_user_password = $row['user_password'];
    $get_user_email = $row['user_email'];
    $get_user_role = $row['user_role'];
    $get_user_image = $row['user_image'];
}

?>

<?php 
    // Updating user entries
    if(isset($_POST['update-user-btn'])) {
        $username = $_POST['username'];
        $user_firstname = $_POST['user-firstname'];
        $user_lastname = $_POST['user-lastname'];

        // Hashing password before sending
        $user_password = $_POST['user-password'];
        $get_salt_query = "SELECT randSalt FROM users";
        $get_salt = mysqli_query($connection,$get_salt_query);
        $salt = mysqli_fetch_assoc($get_salt)['randSalt'];
        $user_password = password_hash($user_password, PASSWORD_BCRYPT, ["cost" => 10]);

        $user_email = $_POST['user-email'];
        $user_role = $_POST['user-role'];

        //Retrieving user profile image
        $user_image = $_FILES['user-image']['name'];
        $user_image_tmp = $_FILES['user-image']['tmp_name'];
        $user_image_size = $_FILES['user-image']['size'];

    

        if($user_image_size > 5 * 1024 * 1024) {
            echo "<div class='alert alert-danger'>The image size cannot be more than 5MB</div>";
        } else {

            if(empty($user_image)) {
                $get_image_query = "SELECT user_image FROM users WHERE user_id = $user_to_edit_id";
                $result = mysqli_query($connection, $get_image_query);
                $user_image = mysqli_fetch_assoc($result)['user_image'];
            }

            move_uploaded_file($user_image_tmp,"../images/$user_image");
    
            //Inserting new user data to the database
            $edit_user_query = "UPDATE users SET ";
            $edit_user_query .= "username = '$username', ";
            $edit_user_query .= "user_firstname = '$user_firstname', ";
            $edit_user_query .= "user_lastname = '$user_lastname', ";
            $edit_user_query .= "user_password = '$user_password', ";
            $edit_user_query .= "user_email = '$user_email', ";
            $edit_user_query .= "user_role = '$user_role', ";
            $edit_user_query .= "user_image = '$user_image' ";
            $edit_user_query .= " WHERE user_id = $user_to_edit_id";
    
            $insertUser = mysqli_query($connection, $edit_user_query); 

            if(!confirmQuery($insertUser)) {
                echo "<div class='alert alert-success'>The user has been updated successfully. <a href='users.php' class='btn btn-info'>Check users</a></div>";
            }
        }

    }

?>



<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="username">Username</label>
        <input value="<?php echo $get_username; ?>" type="text" name="username" class="form-control">
    </div>
    <div class="form-group">
        <label for="user-firstname">Firstname</label>
        <input value="<?php echo $get_user_firstname; ?>" type="text" name="user-firstname" class="form-control">
    </div>
    <div class="form-group">
        <label for="user-lastname">Lastname</label>
        <input value="<?php echo $get_user_lastname; ?>" type="text" name="user-lastname" class="form-control">
    </div>
    <div class="form-group">
        <label for="user-password">Change Password</label>
        <input type="password" name="user-password" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="user-email">Email</label>
        <input value="<?php echo $get_user_email; ?>" type="email" name="user-email" class="form-control">
    </div>
    <div class="form-group">
        <label for="user-role">User Role</label>
        <select name="user-role" class="form-control">
            <option value="" selected>Assign a user role</option>
            <option value="Admin">Admin</option>
            <option value="Editor">Editor</option>
            <option value="Author">Author</option>
            <option value="Subscriber">Subscriber</option>
        </select>
    </div>

    <div class="form-group">
        <label for="user-image">Profile Image</label>
        <input type="file" name="user-image" class="form-control" accept="image/png, image/jpeg">
        <img width="200" src="../images/<?php echo $get_user_image; ?>" alt="" class="img-thumbnail">
    </div>
    <div class="form-group">
        <input type="submit" name="update-user-btn" value="UPDATE" class="btn btn-primary">
    </div>

</form>