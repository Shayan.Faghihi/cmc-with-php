<?php

//edit id is retrieved through url from posts.php - with that we can change and edit the post
if(isset($_GET['edit'])) {
    $edit_post_id = mysqli_real_escape_string($connection,$_GET['edit']);
    $query = "SELECT * FROM posts WHERE post_id=$edit_post_id";
    $results = mysqli_query($connection, $query);
    $row = mysqli_fetch_assoc($results);

    $get_post_title = $row['post_title'];
    $get_post_cat_id = $row['post_category_id'];
    $get_post_author = $row['post_author'];
    $get_post_status = $row['post_status'];
    $get_post_image_url = $row['post_image'];
    $get_post_tags = $row['post_tags'];
    $get_post_content = $row['post_content'];

    // Check if the user is editing his own post else he will be directed back

    if($get_post_author !== $user_logged_in && $user_logged_in !== "Admin") {
        header("Location:posts.php");
        echo $user_logged_in;
    }


    //Operations after changing the post fields

    if(isset($_POST['edit-post-btn'])) {
        $post_title = $_POST['post-title'];
        $post_cat_id = $_POST['post-cat-id'];
        $post_author = $_POST['post-author'];
        $post_status = $_POST['post-status'];
    
        //getting uploaded image
        $post_image = $_FILES['post-image']['name'];
        $post_image_tmp = $_FILES['post-image']['tmp_name'];    //storing image name inside the server as a template
        $post_image_size = $_FILES['post-image']['size'];
    
        $post_tags = $_POST['post-tags'];
        $post_content = $_POST['post-content'];
        $post_date = date('Y-m-d');

    
        //Checking uploaded image size
        if($post_image_size > 5 * 1024 * 1024 ) {
           echo "<div class='alert alert-danger'>You cannot upload image with size more than 1MB</div>";
        } else {
    
            move_uploaded_file($post_image_tmp,"../images/$post_image");

            //Check if the user has uploaded an image or not, if not the previous image will be selected
            if(empty($post_image)) {
                $show_image_query = "SELECT post_image FROM posts WHERE post_id = $edit_post_id";
                $result = mysqli_query($connection, $show_image_query);
                $post_image = mysqli_fetch_assoc($result)['post_image'];
            }
            $edit_post = "UPDATE posts SET 
            post_category_id = '$post_cat_id',
            post_title = '$post_title',
            post_author = '$post_author',
            post_date = '$post_date',
            post_image = '$post_image',
            post_content = '$post_content',
            post_tags = '$post_tags',
            post_status = '$post_status'
            WHERE post_id =  '$edit_post_id'";
            $query = mysqli_query($connection, $edit_post);
    
            if($query) {
                echo "<div class='alert alert-success'>Congratulations! Your post has been edited! View it <a href='../post.php?post-id=$edit_post_id'>now.</a>Or see <a href='posts.php'>all posts</a></div>";
            } else {
                echo "<div class='alert alert-error'>Ooops! Your post editing encountered a problem!</div>" . mysqli_error($query);
            }
    
        }
    
    }
}


?>


<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="post-title">Post Title</label>
        <input type="text" name="post-title" class="form-control" value="<?php echo $get_post_title; ?>">
    </div>
    <div class="form-group">
        <label for="post-cat-id">Post Category</label>
            <select name="post-cat-id" class="form-control">
                
                <?php
                    $category_query = "SELECT * FROM categories";
                    $category_list = mysqli_query($connection, $category_query);
                    while($category = mysqli_fetch_assoc($category_list)) {
                        $cat_id = $category['cat_id'];
                        $cat_title = $category['cat_title'];

                        if($get_post_cat_id == $cat_id) {
                            //showing the selected category first as default 
                            echo "<option selected value='{$cat_id}'>{$cat_title}</option>";
                        } else {
                            echo "<option value='{$cat_id}'>{$cat_title}</option>";
                        }
                        ?>
                    <?php }
                ?>
             </select>
    </div>
    <div class="form-group">
        <label for="post-author">Post Author</label>
        <!-- Showing current user as the default author for the post in the cascading list -->
            <select name="post-author" class="form-control">

                    <option value="<?php echo $get_post_author; ?>"><?php echo $get_post_author; ?></option>
                <?php 
                // Select other users than the current author with an exception
                    $get_users_query = "SELECT * FROM users WHERE NOT username = '$get_post_author'";
                    $get_user = mysqli_query($connection,$get_users_query);

                    while($row = mysqli_fetch_assoc($get_user)) {
                        $username = $row['username'];
                        echo "<option value='$username'>$username</option>";
                    }
                ?>
                
        </select>
    </div>
    <div class="form-group">
        <label for="post-status">Post Status</label>
        <select name="post-status" class="form-control">
            <option value="<?php echo $get_post_status; ?>"><?php echo $get_post_status; ?></option>
            <?php 
                if($get_post_status == "Draft") {
                    echo "<option value='Published'>Published</option>"; 
                } else {
                    echo "<option value='Draft'>Draft</option>"; 
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="post-image">Post Featured Image</label>
        <input type="file" name="post-image" class="form-control" accept="image/png, image/jpeg">
        <img width="300px" src="../images/<?php echo $get_post_image_url; ?>" class="img-thumbnail rounded">
        
    </div>
    <div class="form-group">
        <label for="post-tags">Post Tags</label>
        <input type="text" name="post-tags" class="form-control" value="<?php echo $get_post_tags; ?>">
    </div>
    <div class="form-group">
        <label for="">Post Content</label>
        <textarea name="post-content"  cols="30" rows="10" id="body" class="form-control"><?php echo $get_post_content; ?></textarea>
    </div>
    <div class="form-group">
        <button class="btn btn-primary" type="submit" name="edit-post-btn">EDIT</button>
    </div>

</form>