 <?php  include "includes/header.php"; ?>


    <!-- Navigation -->
    
    <?php  include "includes/navigation.php"; ?>


    <?php 

        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $error_array = array(
                "username" => "",
                "email" => "",
                "password" => ""
            );
            $username= trim($_POST['username']);
            $_SESSION['reg-username'] = $username;

            $email = trim($_POST['email']);
            $_SESSION['reg-email'] = $email;

            $password = password_hash(trim($_POST['password']),PASSWORD_BCRYPT,["cost" => 12]);

            if(!preg_match("/^[a-zA-Z ]*$/", $username)) {
               $error_array['username'] = "<p class='alert alert-danger'>The username can only cotain letters and spaces</p>";
            }
            if(strlen($username) < 4 OR strlen($username) > 15) {
                $error_array['username'] = "<p class='alert alert-danger'>The username length must be between 4 and 15 characters</p>";
            }
            if(user_exists($username)) {
                $error_array['username'] = "<p class='alert alert-danger'>This username is already taken</p>";
            }

            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                if(email_exists($email)) {
                    $error_array['email'] = "<p class='alert alert-danger'>This email already exists. <a href='index.php'>Log in now!</a></p>";
                }
            } else {
               $error_array['email'] = "<p class='alert alert-danger'>The email is not valid</p>";
            }

            foreach ($error_array as $key => $value) {
                if(empty($value)) {
                    unset($error_array[$key]);
                }
            }

            if(empty($error_array)) {

                register_user($username, $email, $password);
            } 
        }
    ?>
    
 
    <!-- Page Content -->
    <div class="container">
    
        <section id="login">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                        <div class="form-wrap">
                        <h1>Register</h1>
                            <form role="form" action="registration.php" method="post" id="login-form" autocomplete="off">
                                <div class="form-group">
                                    <label for="username">username</label>
                                    <input type="text" name="username" id="username" class="form-control" 
                                        placeholder = "Username"  
                                        autocomplete = "on"
                                        value = "<?php echo isset($_SESSION['reg-username']) ? $_SESSION['reg-username'] : '' ?>"
                                    >                             
                                     <?php echo isset($error_array['username']) ? $error_array['username'] : "" ?> <!-- Showing error array -->

                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" 
                                        placeholder = "somebody@example.com" 
                                        autocomplete = "on"
                                        value = "<?php echo isset($_SESSION['reg-email']) ? $_SESSION['reg-email'] : '' ?>"
                                    > 
                                    <?php echo isset($error_array['email']) ? $error_array['email'] : "" ?> <!-- Showing error array -->
                                    
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="key" class="form-control " placeholder="Password" >
                                        
                                   
                                </div>
                                                        
                                <input type="submit" name="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Register">
                            </form>
                        
                        </div>
                    </div> <!-- /.col-xs-12 -->
                </div> <!-- /.row -->
        </section>


        <hr>

        <?php include "includes/footer.php";?>
        
    </div> <!-- /.container -->
 