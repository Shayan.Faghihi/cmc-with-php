    <?php include "includes/header.php"; ?>

    <?php 
     $username = isset($_SESSION['username']) ? $_SESSION['username'] : "";
    ?>

    <!-- Navigation -->
    <?php include "includes/navigation.php"; ?>

  <!-- Geting page number from url -->
    <?php if(isset($_GET['page'])) {
        $page = $_GET['page'];
    } else {
        $page = 1;
    }
    
    ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <h1 class="page-header">
                    Welcome to CMS Project
                   

                </h1>

                <!-- First Blog Post -->

                <?php 

                // Counting the amount of posts
                $limit_query = "SELECT post_display_count FROM posts_options";
                $limit_count_query = mysqli_query($connection,$limit_query);
                $limit = mysqli_fetch_assoc($limit_count_query)['post_display_count'];
                
                if(is_admin($username)) {
                    // Always count and show posts to admin even if not published
                    $posts_count_query = "SELECT * FROM posts";
                    $posts_count = mysqli_query($connection,$posts_count_query);
                    $count = mysqli_num_rows($posts_count);
    
                    $number_of_pages = ceil($count / $limit);  //TO always round up 1.2 -> 2; 1.6 -> 2
                    $offset = (($page - 1)* $limit);            //Offset is one row before the desired row in the database
                
                } else {
                // Only count the published posts to show to the users
                    $posts_count_query = "SELECT * FROM posts WHERE post_status = 'Published'";
                    $posts_count = mysqli_query($connection,$posts_count_query);
                    $count = mysqli_num_rows($posts_count);
    
                    $number_of_pages = ceil($count / $limit);  //TO always round up 1.2 -> 2; 1.6 -> 2
                    $offset = (($page - 1)* $limit);            //Offset is one row before the desired row in the database
                }


                // Check for published posts to show to the users, else show an alert (Always show everything to admin)
                if($count > 0) {

                    // Showing posts as many as the amount of $limit 
                        $query = "SELECT * FROM posts LIMIT $offset, $limit";
                        $show_posts = mysqli_query($connection, $query);
                           
    
                                while($row = mysqli_fetch_assoc ($show_posts)) {
                                    
                                    $post_id = $row['post_id'];
                                     $post_title = $row['post_title'];
                                     $post_author = $row['post_author'];
                                     $post_date = $row['post_date'];
                                     $post_img_link = $row['post_image'];
                                     $post_content = $row['post_content'];
                                     $post_status = $row['post_status'];
                                     $post_view_count = $row['post_view_count'];
            
                                     if($post_status == 'Published' OR is_admin($username)) {
                                      
                                        ?>
            
                                            <h2>
                                                <a href="post.php?post-id=<?php echo $post_id; ?>"><?php echo $post_title; ?></a><small><?php if ($post_status == 'Draft') echo "(Draft)"; ?></small>
                                            </h2>
                                            <p class="lead">
                                                by <a href="author_archive.php?user=<?php echo $post_author;?>"><?php echo $post_author; ?></a>
                                            </p>
                                            <p class="pull-left">
                                                <span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date; ?>
                                                
                                            </p>
                                            <p class=" pull-right">
                                                
                                                <span class="glyphicon glyphicon-eye-open"></span> <?php echo $post_view_count; ?>
                                            </p>
                                      
                                 
                                            <img class="img-responsive img-rounded" src="images/<?php echo $post_img_link; ?>" alt="">
                                          
                                            <p><?php 
                                                $excerpt = substr($post_content,0,200);
                                                echo $excerpt;
                                            ?></p>
                                            <a class="btn btn-primary" href="post.php?post-id=<?php echo $post_id; ?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                                            <hr>
    
                                    <?php } 
                                    
                                } 

                } else {
                //    When there is no post to show to the user
                        echo "<div class='alert alert-warning'>No Posts Available to Show!</div>";
                   
                }

    

                ?>
 
                <!-- Pager and pagination numbers-->
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <?php 
                           if($number_of_pages) {
                               
                                // Disabling the previous button if we are in the first page
                                if($page == 1) {
                                    ?>
                                        <li class="page-item disabled">
                                            <span class="page-link">Previous</span>
                                        </li>
                                <?php } else {
                                    // Showing the previous page for other pages than first page and move it back
                                    ?>
                                        <li class="page-item">
                                            <a class="page-link" href="index.php?page=<?php echo($page - 1); ?>">Previous</a>
                                        </li>
                                <?php }
                            ?>
                            <?php 
                            // Showing all the pagination numbers based on total posts count
                                for($pageCount = 1; $pageCount <= $number_of_pages;$pageCount++) {
                                    if($pageCount == $page) {
                                        ?>
                                        <li class="page-item active"><a class="page-link" href="index.php?page=<?php echo $pageCount; ?>"><?php echo $pageCount;  ?></a></li>
                                    <?php } else {
                                        ?>
                                        <li class="page-item"><a class="page-link" href="index.php?page=<?php echo $pageCount; ?>"><?php echo $pageCount;  ?></a></li>
                                    <?php }
                                }
                            ?>
                            
                            <?php 
                            // showing next page button for every pages except for the last page
                            // After clicking the page number will increase by one
                                if($page < $number_of_pages) {
                                    ?>
                                    <li class="page-item">
                                    <a class="page-link" href="index.php?page=<?php echo ($page + 1); ?>">Next</a>
                                    </li>
                                <?php } else {
                                    ?>
                                    <li class="page-item disabled">
                                    <span class="page-link">Next</span>
                                    </li>
                                <?php }
                           } 
                        ?>
                     
                        
                    </ul>
                </nav>
            </div>

            <!-- Blog Sidebar Widgets Column -->
           <?php include_once "includes/sidebar.php"; ?>


           

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
       <?php include_once "includes/footer.php"; ?>