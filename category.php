<?php include "includes/header.php"; ?>

<?php 
    $username = isset($_SESSION['username']) ? $_SESSION['username'] : "";
?>

<!-- Navigation -->
<?php include "includes/navigation.php"; ?>

<!-- Retrieving category details with id coming from url -->
<?php
    if(isset($_GET['cat-id'])) {
        $catId = $_GET['cat-id'];

        if(is_admin($username)) { 
            $stmt1 = mysqli_prepare($connection,"SELECT post_id, post_title, post_author, post_date, post_image, post_content, post_status FROM posts WHERE post_category_id = ?") ;

        } else {
            
            $stmt2 = mysqli_prepare($connection,"SELECT post_id, post_title, post_author, post_date, post_image, post_content, post_status FROM posts WHERE post_category_id = ? AND post_status=?") ;
            $published = "Published";
        }


        if(isset($stmt1)) {
          mysqli_stmt_bind_param($stmt1,"i", $catId);
          mysqli_stmt_execute($stmt1);
            
          mysqli_stmt_bind_result($stmt1,$post_id, $post_title, $post_author, $post_date, $post_image, $post_content, $post_status);  
            $stmt = $stmt1;

        } else {
            mysqli_stmt_bind_param($stmt2,"is",$catId, $published);
            mysqli_stmt_execute($stmt2); 

            mysqli_stmt_bind_result($stmt2,$post_id, $post_title, $post_author, $post_date, $post_image, $post_content, $post_status);
            $stmt = $stmt2;
        }
        

    }

?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                Page Heading
                <small>Secondary Text</small>
            </h1>

            <!-- First Blog Post -->
            <?php 
                if(mysqli_stmt_num_rows($stmt) === 0) {
                        //    When there is no post to show to the user
                    echo "<div class='alert alert-warning'>No Posts Available to Show!</div>";
                } 

                while(mysqli_stmt_fetch($stmt)) :       
                        ?>
                            <h2>
                                <a href="post.php?post-id=<?php echo $post_id; ?>"><?php echo $post_title; ?></a>
                            </h2>
                            <p class="lead">
                                by <a href="index.php"><?php echo $post_author; ?></a>
                            </p>
                            <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date; ?></p>
                            <hr>
                            <img class="img-responsive img-rounded" src="images/<?php echo $post_image; ?>" alt="">
                            <hr>
                            <p><?php 
                                $excerpt = substr($post_content,0,200);
                                echo $excerpt;
                            ?></p>
                            <a class="btn btn-primary" href="post.php?post-id=<?php echo $post_id; ?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                            <hr>
                
                <?php endwhile; mysqli_stmt_close($stmt);   
   
            ?>

            <!-- Pager -->
            <ul class="pager">
                <li class="previous">
                    <a href="#">&larr; Older</a>
                </li>
                <li class="next">
                    <a href="#">Newer &rarr;</a>
                </li>
            </ul>

        </div>

        <!-- Blog Sidebar Widgets Column -->
       <?php include "includes/sidebar.php"; ?>

    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->
   <?php include "includes/footer.php";?>