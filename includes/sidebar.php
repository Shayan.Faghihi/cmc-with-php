

<div class="col-md-4">

<!-- Blog Search Well -->
<div class="well">
    <h4>Blog Search</h4>
    <form action="search.php" method="post">
        <div class="input-group">
            <input name="search-term" type="text" class="form-control">
            <span class="input-group-btn">
                <button name="search-btn" class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div>
    </form>
    <!-- /.input-group -->
</div>

<!-- Login Form in sidebar -->

        <?php 
        
        if(!isset($_SESSION['username'])) {
            ?>

            <div class="well">
                <h4>Login</h4>
                <form action="includes/login.php" method="post">
                    <div class="form-group">
                        <input type="text" name="login-username" class="form-control" placeholder="Enter Username">
                    </div>
                    <div class="input-group">
                        <input type="password" name="login-password" class="form-control" placeholder="Enter Password">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" name="user-login-btn">Login</button>
                        </span>
                    </div>
                </form>

            </div>

        <?php } else {
            ?>
            <div class="well">
                <h4>Logged in as <?php echo $_SESSION['username']; ?></h4>
                <div><a class="btn btn-primary" href="logout.php">Logout</a></div>
            </div>
        <?php }
        
        ?>
<!-- Blog Categories Well -->
<div class="well">
    <h4>Blog Categories</h4>
    <div class="row">
        <div class="col-lg-6">
            <ul class="list-unstyled">
                <?php 
                    $query = "SELECT * FROM categories";
                    $cat_query = mysqli_query($connection, $query);

                    while($row = mysqli_fetch_assoc($cat_query)) {
                        $catTitle = $row['cat_title'];
                        $catId = $row['cat_id'];
                        
                        echo "<li><a href='category.php?cat-id=$catId'>". $catTitle . "</a></li>";
                        
                    }

                ?>
            </ul>
        </div>
        <!-- /.col-lg-6 -->
        <!-- <div class="col-lg-6">
            <ul class="list-unstyled">
                <li><a href="#">Category Name</a>
                </li>
                <li><a href="#">Category Name</a>
                </li>
                <li><a href="#">Category Name</a>
                </li>
                <li><a href="#">Category Name</a>
                </li>
            </ul>
        </div> -->
        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->
</div>

<!-- Side Widget Well -->
<div class="well">
    <h4>Side Widget Well</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
</div>

</div>