
<?php 
// check the url
   $file_url = basename($_SERVER['PHP_SELF'],".php"); 

?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">CMS Front</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="nav-item <?php if ($file_url == 'about') echo 'active'; ?>">
                        <a href="#">About</a>
                    </li>
                    <li class="nav-item  <?php if ($file_url == 'services') echo 'active'; ?>">
                        <a href="#">Services</a>
                    </li>
                    <li class="nav-item  <?php if ($file_url == 'contact') echo 'active'; ?>">
                        <a href="contact.php">Contact</a>
                    </li>
            
                    <?php 
                    // Admin Link only when is logged in
                        if(isset($_SESSION['user_role'])) {
                            if($_SESSION['user_role'] == "Admin") {

                                ?>
                            <li>
                                <a href="admin">Admin</a>
                            </li>

                            <?php }

                        }
                    ?>

                    <?php 
                    // Logout link only when is logged in
                        if(isset($_SESSION['username'])) {
                            ?>
                            <li>
                                <a href="logout.php">Logout</a>
                            </li>
                        <?php }
                    ?>

                    <li class="<?php if($file_url == 'registration') echo 'active' ?>">
                    <?php 
                        if(!isset($_SESSION['username'])) {
                            echo "<a href='registration.php'>Register</a>";

                        }
                    
                    ?>
                        
                    </li>

                    
                    <?php
                    if(isset($_SESSION['username'])) {
                        // Check the URL
                        if(basename($_SERVER['SCRIPT_NAME']) == 'post.php') {
    
                            echo "<li><a href='admin/posts.php?source=edit-post&edit={$_GET['post-id']}'>Edit Post</a></li>";
                        } 

                    }
                    
                    ?>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>