<?php include "includes/header.php"; ?>
<?php
 if(isset($_SESSION['user_role'])){
    $user_role = $_SESSION['user_role'];
} else {
    $user_role = "";
}
?>

<!-- Navigation -->
<?php include "includes/navigation.php"; ?>

<?php 
// Retrieving user generated content
    if(isset($_GET['user'])) {
        $username = $_GET['user'];
    }
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                Posts of "<?php echo $username; ?>"
            </h1>

            <!-- First Blog Post -->

            <?php 

            //Showing all posts of $username
                $query = "SELECT * FROM posts WHERE post_author = '$username' ORDER BY post_id DESC";
                $show_posts = mysqli_query($connection, $query);

                    while($row = mysqli_fetch_assoc($show_posts)) {
                        $post_id = $row['post_id'];
                         $post_title = $row['post_title'];
                         $post_author = $username;
                         $post_date = $row['post_date'];
                         $post_img_link = $row['post_image'];
                         $post_content = $row['post_content'];
                         $post_status = $row['post_status'];

                        //  Showing only the published posts
                         if($post_status == 'Published' OR $user_role == "Admin") {

                            ?>

                                <h2>
                                    <a href="post.php?post-id=<?php echo $post_id; ?>"><?php echo $post_title; ?></a><small><?php if ($post_status == 'Draft') echo "(Draft)"; ?></small>
                                </h2>
                                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date; ?></p>
                                <hr>
                                <img class="img-responsive img-rounded" src="images/<?php echo $post_img_link; ?>" alt="">
                                <hr>
                                <p><?php 
                                    $excerpt = substr($post_content,0,200);
                                    echo $excerpt;
                                ?></p>
                                <a class="btn btn-primary" href="post.php?post-id=<?php echo $post_id; ?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                                <hr>
                        <?php } 
                        
                    } 
   
            ?>

          
            <!-- Pager -->
            <ul class="pager">
                <li class="previous">
                    <a href="#">&larr; Older</a>
                </li>
                <li class="next">
                    <a href="#">Newer &rarr;</a>
                </li>
            </ul>

        </div>

        <!-- Blog Sidebar Widgets Column -->
       <?php include_once "includes/sidebar.php"; ?>

    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->
   <?php include_once "includes/footer.php"; ?>